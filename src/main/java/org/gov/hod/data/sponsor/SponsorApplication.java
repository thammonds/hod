package org.gov.hod.data.sponsor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity(name = "sponsor_application")
@Access(AccessType.FIELD)
class SponsorApplication implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "sponsor_name")
    @NotNull
    private String sponsorName;

    @Column(name = "application_id")
    private String applicationId;

    @Column(name = "company_number")
    private String companyNumber;

    SponsorApplication() { }

    void generateApplicationId() {
        applicationId = Long.toString(System.currentTimeMillis());
    }

    public Long getId() {
        return id;
    }

    public String getSponsorName() {
        return sponsorName;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public String getCompanyNumber() {
        return companyNumber;
    }

    public void setCompanyNumber(String companyNumber) {
        this.companyNumber = companyNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SponsorApplication that = (SponsorApplication) o;
        return Objects.equals(applicationId, that.applicationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(applicationId);
    }
}
