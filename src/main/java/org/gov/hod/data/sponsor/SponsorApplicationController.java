package org.gov.hod.data.sponsor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SponsorApplicationController {

    @Autowired
    private SponsorApplicationRepository repository;

    @PostMapping("/sponsor")
    @Transactional
    public Long createSponsorApplication(@RequestBody SponsorApplication sponsorApplication) {
        sponsorApplication.generateApplicationId();
        SponsorApplication entity = repository.save(sponsorApplication);
        return entity.getId();
    }

    @PutMapping("/sponsor/{id}")
    @Transactional
    public void addCompanyNumber(@PathVariable long id, @RequestBody String companyNumber) {
        SponsorApplication existingApplication = repository.findOne(id);
        existingApplication.setCompanyNumber(companyNumber);
    }

    @GetMapping("/sponsor")
    public List<SponsorApplication> getApplications() {
        return (List<SponsorApplication>) repository.findAll();
    }

    @GetMapping("/sponsor/{id}")
    public SponsorApplication getApplication(@PathVariable("id") long id) {
        return repository.findOne(id);
    }
}
