package org.gov.hod.data.sponsor;

import org.springframework.data.repository.CrudRepository;

public interface SponsorApplicationRepository extends CrudRepository<SponsorApplication, Long> {
}
