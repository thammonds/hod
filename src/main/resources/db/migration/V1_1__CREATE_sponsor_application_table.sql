create table sponsor_application
(
    id SERIAL PRIMARY KEY,
    sponsor_name VARCHAR(255) NOT NULL,
    application_id VARCHAR(255) NOT NULL
)